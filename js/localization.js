var localization = {
	// Define language strings
	ENGLISH: 0,
	SPANISH: 1,

	// Current language
	currentLanguage: 0, // ENGLISH default

	get: function(id) {
		return this.strings[ id ][ this.currentLanguage ];
	},

	getCamelCase: function(id) {
		var str = this.get(id);
		return str.toLowerCase().replace(/(?:^|\s)\w/g, function(match) {
	        return match.toUpperCase();
	    });
	},

	strings: {
		tituloCalculador: ["Risk calculator", "Calculador de riesgo"],
		tituloRecomendaciones: ["Recommendations", "Recomendaciones"],
		tituloMasaCorporal: ["Body mass index", "Masa corporal"],

		masRecomendaciones: ["More recommendations", "Más recomendaciones"],
		tituloFormulario: ["Enter your information and press Calculate:", "Ingrese sus datos y presione Calcular:"],
		sexo: ["Gender", "Género"],
		edad: ["Age", "Edad"],
		tabaquismo: ["Smoker", "Tabaquismo"],
		nombre: ["Name", "Nombre"],
		tensionSistolica: ["Systolic blood pressure (mmHg)", "Presión Máxima (sistólica) mmHg"],
		diabetes: ["Diabetes", "Diabetes"],
		colesterol: ["Cholesterol (mg/dl)", "Colesterol total (mg/dl)"],
		hombre: ["MALE", "MASCULINO"],
		mujer: ["FEMALE", "FEMENINO"],
		si: ["YES", "SI"],
		no: ["NO", "NO"],
		nsnc: ["I don't know", "No lo sé"],
		calcular: ["Calculate", "Calcular"],
		recalcular: ["Recalculate", "Recalcular"],
		datosIngresados: ["Input data", "Datos ingresados"],
		resultados: ["RESULTS", "RESULTADOS"],
		riesgo10anos: ["10 year risk of cardiovascular event", "Riesgo cardiovascular a 10 años"],
		peso: ["Weight (kg)", "Peso (kg)"],
		altura: ["Height (cm)", "Altura (cm)"],
		quePasaria: ["What if...", "Qué pasaría si..."],


		// Recomendaciones basicas
		recomendacionBasicaRiesgo1: [
			"Individuals in this category are at low risk. Low risk does not mean \“no\” risk.<br>Conservative management focusing on lifestyle interventions is suggested.",
			"Los individuos de esta categoría tienen un riesgo bajo. Un bajo riesgo no significa \“ausencia de riesgo\”.<br>Se sugiere un manejo discreto centrado en cambios del modo de vida."
		],

		recomendacionBasicaRiesgo2: [
			"Individuals in this category are at moderate risk of fatal or non-fatal vascular events.<br>Monitor risk profile every 6–12 months.",
			"Los individuos de esta categoría tienen un riesgo moderado de sufrir episodios cardiovasculares, mortales o no.<br>Monitorización del perfil de riesgo cada 6-12 meses."
		],

		recomendacionBasicaRiesgo3: [
			"Individuals in this category are at high risk of fatal or non-fatal vascular events.<br>Monitor risk profile every 3–6 months.",
			"Los individuos de esta categoría tienen un riesgo alto de sufrir episodios cardiovasculares, mortales o no.<br>Monitorización del perfil de riesgo cada 3-6 meses."
		],

		recomendacionBasicaRiesgo4: [
			"Individuals in this category are at very high risk of fatal or non-fatal vascular events.<br>Monitor risk profile every 3–6 months.",
			"Los individuos de esta categoría tienen un riesgo muy alto de sufrir episodios cardiovasculares, mortales o no.<br>Monitorización del perfil de riesgo cada 3-6 meses."
		]
	}

};
