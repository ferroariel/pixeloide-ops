// Calculador Cardíaco
//
// Autor: Pixeloide, Inc. @2014
//

var app = {
    // Variables
    mBasePath: null,
    mCVDPages: [],
    mRecomendacionIndice: null,

    // Application Constructor
    initialize: function() {
        //var isApp = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
		var isApp = false;
        if ( isApp )
        {
            this.bindEvents();
        }
        else
        {
            this.iniciar();
        }
    },
    
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    
    // deviceready Event Handler
    //
    // The scope of 'this' is the event.
    onDeviceReady: function() {
        app.iniciar();
    },

    basePath: function() {
        if(this.mBasePath == null) {
            this.mBasePath = window.location.href;
            this.mBasePath = this.mBasePath.replace(this.mBasePath.substr(this.mBasePath.lastIndexOf('/') + 1), '');
        }
        return this.mBasePath;
    },

    iniciar: function() {
        this.showTab(0);
        this.loadDivs(function() {
            if(navigator.splashscreen != undefined) {
                window.setTimeout(function() {
                    navigator.splashscreen.hide();
                }, 1000);
            }
        });
    },

    loadExternalDiv: function(url, callback) {
        var div = $("<div/>").load(url, function() {
            var divUnwrapped = div.children().first();
            callback( divUnwrapped );
        });
    },

    loadDivs: function(onComplete) {
        // Localizacion de tabs
        $("#tabLinkCalculador").html( localization.get("tituloCalculador") );
        $("#tabLinkRecomendaciones").html( localization.get("tituloRecomendaciones") );
        $("#tabLinkMasaCorporal").html( localization.get("tituloMasaCorporal") );

        this.loadExternalDiv(this.basePath() + "calculadorRiesgo.html #paginaCalculador", function(newDiv){
            app.iniciarFormularioCVD(newDiv);
            
            app.pushCVDPage(newDiv);

            onComplete();
        });

        $("#tab1").load(this.basePath() + "recomendaciones.html #paginaIndice", function(){
            app.inicializarRecomendaciones();
        });

        $("#tab2").load(this.basePath() + "masaCorporal.html #paginaMasa", function(){
            app.inicializarMasaCorporal();
        });
		
		//agrego la tab del reminder
		/*  $("#tab3").load(this.basePath() + "lista_boton_agregar.html .listaBotonAgregar", function(){
            app.inicializarReminder();
        });*/
		
		
    },

    showTab: function(tabId) {
        for(var i = 0; (element = $("#tab" + i)).length > 0; i++)
        {
            if(i == tabId)
                element.show();
            else
                element.hide();
        }
    },

    pushCVDPage: function(newPage) {
        if(this.mCVDPages.length > 0)
        {
            var currentPage = this.mCVDPages[this.mCVDPages.length - 1];

            this.performPageTransition($("#tab0"), currentPage, newPage, false);
        }
        else
        {
            newPage.addClass("ui-active-page");

            $("#tab0").empty();
            $("#tab0").append(newPage);
        }
        this.mCVDPages.push(newPage);
    },

    popCVDPage: function() {
        var currentPage = this.mCVDPages.pop();
        var oldPage = this.mCVDPages[this.mCVDPages.length-1];

        this.performPageTransition($("#tab0"), currentPage, oldPage, true);
    },

    performPageTransition: function(container, from, to, back) {
        container.append(to);

        from.show();
        to.show();

        from.addClass('slide out');
        to.addClass('slide in');

        from.removeClass("ui-active-page");
        to.addClass("ui-active-page");

        if (back) {
            from.addClass('reverse');
            to.addClass('reverse');
        }

        window.setTimeout(function() {
            from.removeClass('slide out');
            to.removeClass('slide in');
            from.removeClass('reverse');
            to.removeClass('reverse');

            from.remove();
        }, 500);
    },

    iniciarFormularioCVD: function(container) {
        container.find("#enunciado").html( localization.get("tituloFormulario") );
        container.find("#sexoTitulo").html( localization.get("sexo") + ":" );
        container.find("#edadTitulo").html( localization.get("edad") + ":" );
        container.find("#tabaquismoTitulo").html( localization.get("tabaquismo") + ":" );
        container.find("#tensionSistolicaTitulo").html( localization.get("tensionSistolica") + ":" );
        container.find("#diabetesTitulo").html( localization.get("diabetes") + ":" );
        container.find("#colesterolTitulo").html( localization.get("colesterol") + ":" );

        container.find("label[for='sexo']").attr("data-on", localization.get("hombre"));
        container.find("label[for='sexo']").attr("data-off", localization.get("mujer"));

        container.find("label[for='tabaquismo']").attr("data-on", localization.get("si"));
        container.find("label[for='tabaquismo']").attr("data-off", localization.get("no"));

        container.find("label[for='diabetes']").attr("data-on", localization.get("si"));
        container.find("label[for='diabetes']").attr("data-off", localization.get("no"));

        container.find("#botonCalcular").val( this.mCVDPages.length == 0 ? localization.get("calcular") : localization.get("recalcular") );
        container.find("#masRecomendaciones").html( localization.get("masRecomendaciones") );
        container.find("#quePasariaTitulo").html( localization.get("quePasaria") );

        var edadSelect = container.find("#edad");
        edadSelect.empty();
        for(var edad = 40; edad <= 80; edad++) {
            edadSelect.append("<option value='" + edad + "'>" + edad + "</select>");
        }

        var colesterolSelect = container.find("#colesterol");
        colesterolSelect.empty();
        colesterolSelect.append("<option value='nsnc'>" + localization.get("nsnc") + "</option>");
        for(var colesterol = 100; colesterol <= 450; colesterol++) {
            colesterolSelect.append("<option value='" + colesterol + "'>" + colesterol + "</option>");
        }

        var tensionSelect = container.find("#tensionSistolica");
        tensionSelect.empty();
        for(var tension = 70; tension <= 260; tension++) {
            tensionSelect.append("<option value='" + tension + "'>" + tension + "</option>");
        }
    },

    inicializarRecomendaciones: function() {
        $("#tab1 #headerH1").html( localization.get("tituloRecomendaciones").toUpperCase() );

        // Show only the valid language
        $("#tab1 #copete p").hide();
        $("#tab1 #indice li div").hide();

        var idToShow = localization.currentLanguage + 1;
        $("#tab1 #copete p:nth-child("+idToShow+")").show();
        $("#tab1 #indice li div:nth-child("+idToShow+")").show();
    },

    inicializarMasaCorporal: function() {
        $("#tab2 #pesoTitulo").html( localization.get("peso") );
        $("#tab2 #alturaTitulo").html( localization.get("altura") );
        $("#tab2 #tituloResultadoMasa").html( localization.get("tituloMasaCorporal") );

        var alturaSelect = $("#tab2 #altura");
        alturaSelect.empty();
        for(var altura = 1.50; altura < 2.31; altura += 0.01)
        {
            var alturaVal = Math.floor(altura * 100) / 100;
            alturaSelect.append("<option value='" + alturaVal + "'>" + alturaVal + "</option>");
        }

        var pesoSelect = $("#tab2 #peso");
        pesoSelect.empty();
        for(var peso = 50; peso < 230; peso++)
        {
            var pesoVal = Math.floor(peso * 100) / 100;
            pesoSelect.append("<option value='" + pesoVal + "'>" + pesoVal + "</option>");
        }
    },
	
	
	//inicializa reminder:
	
	 inicializarReminder: function() {
     //insertar contenido
    },

    onCalculateButtonClicked: function() {
        // edad: Integer
        // colestero: Integer (puede ser null)
        // tension: Integer
        // tabaquismo: true/false
        // sexoMasc: true==masculino / false==femenino
        // diabetes: true/false
        var edad = $(".ui-active-page #edad").val();
        var sexo = $(".ui-active-page #sexo").prop("checked");
        var colesterol = $(".ui-active-page #colesterol").val();
        if(colesterol == "nsnc")
            colesterol = null;
        var tension = $(".ui-active-page #tensionSistolica").val();
        var tabaquismo = $(".ui-active-page #tabaquismo").prop("checked");
        var diabetes = $(".ui-active-page #diabetes").prop("checked");
        
        var resultado = Calculador.obtenerRiesgo(edad, colesterol, tension, tabaquismo, sexo, diabetes);

        var oldDiv = $("#tab0 #paginaCalculador");
        app.loadExternalDiv(this.basePath() + "resultados.html #paginaResultados", function(newDiv) {

            app.iniciarFormularioCVD(newDiv);

            // Show color table
            newDiv.find("#labelRisk1").hide();
            newDiv.find("#labelRisk2").hide();
            newDiv.find("#labelRisk3").hide();
            newDiv.find("#labelRisk4").hide();
            newDiv.find("#labelRisk5").hide();

            newDiv.find("#labelRisk" + resultado).show();

            // Copy attributes
            newDiv.find("#sexo").prop("checked", sexo);
            newDiv.find("#edad").val(edad);
            newDiv.find("#colesterol").val(colesterol != null ? colesterol : "nsnc");
            newDiv.find("#tensionSistolica").val(tension);
            newDiv.find("#tabaquismo").prop("checked", tabaquismo);
            newDiv.find("#diabetes").prop("checked", diabetes);

            // Header resultado
            newDiv.find("#riesgoTitulo").html( localization.get("riesgo10anos") );
            
            var strRecomendacion = app.getTextoRecomendacion(resultado, sexo, edad, colesterol, tension, tabaquismo, diabetes);
            newDiv.find("#riesgoRecomendacion").html( strRecomendacion );

            // Datos ingresados
            newDiv.find("#headerH1").html( localization.get("resultados") );
            newDiv.find("#tituloDatosIngresados").html( localization.get("datosIngresados") );

            var datosIngresados = newDiv.find("#datosingresados");
            datosIngresados.empty();
            datosIngresados.append( "<li>" + localization.get("sexo") + ": <span>" + localization.getCamelCase(sexo ? "hombre" : "mujer") + "</span></li>" );
            datosIngresados.append( "<li>" + localization.get("edad") + ": <span>" + edad + "</span></li>" );
            if(colesterol)
                datosIngresados.append( "<li>" + localization.get("colesterol") + ": <span>" + colesterol + "</span></li>" );
            datosIngresados.append( "<li>" + localization.get("tensionSistolica") + ": <span>" + tension + "</span></li>" );
            datosIngresados.append( "<li>" + localization.get("tabaquismo") + ": <span>" + localization.getCamelCase(tabaquismo ? "si" : "no") + "</span></li>" );
            datosIngresados.append( "<li>" + localization.get("diabetes") + ": <span>" + localization.getCamelCase(diabetes ? "si" : "no") + "</span></li>" );

            newDiv.find("#li-sexo").hide();
            newDiv.find("#li-edad").hide();
            newDiv.find("#li-diabetes").hide();

            app.pushCVDPage(newDiv);	
        });
    },

    getTextoRecomendacion: function(riesgo, sexo, edad, colesterol, tension, tabaquismo, diabetes) {
        var recomendacion = "";

        switch(riesgo)
        {
            case 1:
                recomendacion += localization.get("recomendacionBasicaRiesgo1");
                break;
            case 2:
                recomendacion += localization.get("recomendacionBasicaRiesgo2");
                break;
            case 3:
                recomendacion += localization.get("recomendacionBasicaRiesgo3");
                break;
            case 4:
            case 5:
                recomendacion += localization.get("recomendacionBasicaRiesgo4");
                break;
        }

        return recomendacion;
    },

    computeBMI: function() {
        var peso = $("#tab2 #peso").val();
        var altura = $("#tab2 #altura").val();

        var masaCorporal = peso / (altura * altura);
        masaCorporal = Math.floor(masaCorporal * 100) / 100;

        $("#tab2 #resultadoMasaCorporal").html(masaCorporal);
    },

    showDetalleRecomendacion: function(idRecomendacion) {
        var oldDiv = $("#tab1 #paginaIndice");
        mRecomendacionIndice = oldDiv;

        app.loadExternalDiv(this.basePath() + "recomendaciones.html #paginaDetalle", function(newDiv){
            // Oculto todas las recomendaciones
            var recomendaciones = newDiv.find(".contenidoRecomendaciones");
            recomendaciones.hide();

            // Muestro solo la que corresponde
            var divIndex = 2 * idRecomendacion + localization.currentLanguage;
            recomendaciones.eq(divIndex).show();

            app.performPageTransition($("#tab1"), oldDiv, newDiv, false);
        });
    },

    goBackRecomendacion: function() {
        var oldDiv = $("#tab1 #paginaDetalle");
        var newDiv = mRecomendacionIndice;

        app.performPageTransition($("#tab1"), oldDiv, newDiv, true);
    }
};
